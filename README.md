## Morpheus Quick Shots

### What is it? 

As prospects and customers are looking at the MorpheusData platform there are a lot of questions around examples.
The following repo will contain some examples of tasks / workflows / scripts that you can use that have been tested with and validiate. 

The goal is to create a python script you can copy into a morpheus appliance and install all fo the examples.

### What it's not?
The goal is not to build custom automation for a customer or prospect its to demonstrate capabilities.  These uses cases are free for you to test and example, but we stop short of support with these scripts.  The environment descriptions and versions will be included.  The scripts were tested with that environment and work for that environment. 


## Capabilities and Versions
### Tested Versions
| Component | Version |
|----------| --------| 
| Morpheus | 6.0.1 | 
| Ansible | 0 | 
| Python | 0 | 
| Terraform | 0 | 


Ansible Core 
---

Ubuntu 22.04, 20.04
- Ansible Task (repo Morpheus Command Bus checked) Wordpress Install Ubuntu 22.04, 20.04
- Ansible Task (repo Morpheus Command Bus unchecked) Wordpress Install Ubuntu 22.04, 20.04
- Ansible Task (repo Morpheus Command Bus checked) Patch Management - Update Servers with specific label
- Ansible Task (repo Morpheus Command Bus unchecked) Patch Management - Update Servers with specific label
- Ansible Task (repo Morpheus Command Bus checked) Install Services and Apps - As one off or with a label
- Ansible Task (repo Morpheus Command Bus unchecked) Install Services and Apps - As one off or with a label
--> Option types and inputs? Possible? 

Python
---

