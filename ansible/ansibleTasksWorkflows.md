
### What is Ansible?
Ansible is a configuration management engine that is rapidly growing in popularity in the IT and DevOPS community. While it lacks some of the benefits at scale that solutions such as Salt, Chef, or Puppet offer. It is very easy to get started and allows engineers to develop tasks in a simplistic markup language known as YAML. Morpheus integrates with an existing repository of playbooks as the master in a master-slave Ansible architecture.

Morpheus not only supports Ansible but greatly enhances Ansible to do things that it could not do in its native form. For example, Ansible can now be configured to run over the Morpheus agent communication bus. This allows playbooks to be run against instances where SSH/WinRM access may not be feasible due to networking restrictions or other firewall constraints. Instead it can run over the Morpheus Agent which only requires port 443 access back to the Morpheus appliance URL.

This integration supports both Linux-based and Windows platforms for playbook execution and can also be configured to query secrets from Morpheus Cypher services (similar to Vault).

https://docs.morpheusdata.com/en/latest/integration_guides/Automation/ansible.html?highlight=ansible

### Why use the command bus? 

In many environments, there may be security restrictions on utilizing SSH or WinRM to run playbooks from an Ansible server on the appliance to a target machine. This could be due to being a customer network (in the environment of an MSP ), or various security restrictions put in place by tighter industries (i.e. Government, Medical, Finance).

Ansible can get one in trouble in a hurry. It is limited in scalability due to its fundamental design decisions that seem to bypass concepts core to all other configuration management frameworks (i.e. SaltStack, Chef, and Puppet). Because of its lack of an agent, the Ansible execution binary itself has to handle all the load and logic of executing playbooks on all the machines in the inventory of an Ansible project. This differs from other tools where the workload is distributed across the agents of each vm. Because of this (reaching out) approach, Ansible is very easy to get started with, but can be quite a bit slower as well as harder to scale up. However, Morpheus offers some solutions to help mitigate these issues and increase scalability while, at the same time improving security.

One of the great things about Morpheus is it’s Agent Optional approach. This means that this functionality can work without the Agent, however the agent is what adds the security benefits being represented here. When an instance is provisioned (or converted to managed) within Morpheus, an agent can be installed. This agent opens a secure websocket back to the Morpheus appliance (over port 443). This agent is responsible for sending back logs, guest statistics, and a command bus for automation. Since it is a WebSocket, bidirectional communication is possible over a STOMP communication bus.

When this functionality is enabled on an Ansible integration, a connection_plugin is registered with Ansible of type morpheus and morpheus_win. These direct bash or powershell commands, in their raw form, from Ansible to run over a Morpheus api. The Ansible binary sends commands to be executed as an https request over the API utilizing a one time execution lease token that is sent to the Ansible binary. File transfers can also be enacted by this API interface. When Morpheus receives these commands, they are sent to the target instances agent to be executed. Once they have completed a response is sent back and updated on the ExecutionRequest within Morpheus. Ansible polls for the state and output on these requests and uses those as the response of the execution. This means Ansible needs zero knowledge of a machines target ip address, nor its credentials. These are all stored and safely encrypted within Morpheus.

It has also been pointed out that this execution bus is dramatically simpler than utilizing pywinrm when it comes to orchestrating Windows as the winrm configurations can be cumbersome to properly setup, especially in tightly secured Enterprise environments.

### Whats included in this QuickHit? 

#### Native Ansible Communication
- Morpheus Tasks that install Wordpress on a Ubuntu Instance
- Morpheus Provisioning Workflow that contains the task that can be assigned at provisioning  
- Playbooks are configured properly to leverage native Ansible communication

#### Morpheus Agent as the Command-bus
- Morpheus Tasks that install Wordpress on a Ubuntu Instance
- Morpheus Provisioning Workflow that contains the task that can be assigned at provisioning
- Playbooks that are configured properly to leverage the Morpheus Agent as the command-bus

---
#### **IMPORTANT** 
Ensure you have the following installed on the Ubuntu Appliance `sudo apt install python3-virtualenv`

---

### How to use the newly setup tasks and workflows? 

---

#### Use Case: Install Software on an existing instance.

##### Who would choose this option? 
- End User: 
	- An end user might choose this option to install the software of their choosing that they have permissions to see the specific tasks. 
	- They may want to layer on multiple applications at different times.  They may start with a database, and later on layer on another configuration or application.
	- They may want to run a different Ansible script that changes the environment settings - since Ansible can be used as configuration management.  
- Admin / IT Ops / SecOps
	- They may have patches they want installed on the system before handed over to the end user. 
	- Security team may have needs to lock down a firewall from the system side of things. 
	- The company may have a supported configuration for all servers that need to be run against the host before they allow end users to leverage the server. 
	- If they need to run a task to remediate issues with a server. 

In many cases, a team may want to install specific software on an instance thats been deployed.  The IT team may have requirements they must meet during the install of that application.  Items might include but are not limited to: security requirements, versions of specific applications, certain configuration designs from a platform aspect. 

Using the Tasks that have been provided there are two ways to leverage them.

1. Selecting an instance thats already been provisioned
2. In the top right corner of the instance page, select `Actions` and then select `Run Task`
3. Select the task you wish to run. 

##### Chose your adventure: Do you run with native Ansible Comms or do you run with Morpheus Agent as a command bus?

Both will function similarly in the sense that they will both complete the objective of installing Wordpress.  The major difference is the method or the connection they use with Ansible to communicate with the Instance that is provisioned. 

---

#### Use Case: A provisioning workflow. 

##### Who would choose this option? 

- End User: 
	- During the provisioning process of a Ubuntu server they may want to install Wordpress.  They can choose at the time of provisioning.  
	- If they already have a ubuntu server maybe there is a select set of tasks or collection of tasks that they want to run against an instance. 
- Admin / IT Ops / SecOps
	- The IT Ops team may have a supported Wordpress workflow that they attach to the instance type.  By doing this it ensures that every time the wordpress server is deployed it gets all of the supported configurations. 
	- SecOps - may need to endpoint security agents are installed at the time they are delivered to the end customer. 
	- IT Ops may need to update an instance with the latest supported software versions or enforce compliance of a specific configuration. 


#### Use Case:  A catalog item that installs word press. 

